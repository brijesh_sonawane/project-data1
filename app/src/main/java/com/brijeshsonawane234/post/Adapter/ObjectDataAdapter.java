package com.brijeshsonawane234.post.Adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.brijeshsonawane234.post.Activity.MainActivity;
import com.brijeshsonawane234.post.Pojo.Data;
import com.brijeshsonawane234.post.Pojo.Image;
import com.brijeshsonawane234.post.Pojo.ObjectData;
import com.brijeshsonawane234.post.R;
import com.bumptech.glide.Glide;

import java.util.Arrays;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class ObjectDataAdapter extends RecyclerView.Adapter<ObjectDataAdapter.MyViewHolder>
{
    List<ObjectData> objectDataList;
    MainActivity mainActivity;

    public ObjectDataAdapter(List<ObjectData> objectDataList,MainActivity mainActivity)
    {

        this.objectDataList = objectDataList;
        this.mainActivity=mainActivity;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType)
    {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row,parent,false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position)
    {
        ObjectData objectData= objectDataList.get(position);
        List<Data> data= objectData.getData();
        List<Image> images=null;
        int count=0;
        for (Data d: data)
        {

            if (d.getImages() != null)
            {
                Log.d("data1",""+d.getImages().length);
                holder.textView.setText(objectData.getSection_header());
                images= Arrays.asList(d.getImages());
                if (count == 0)
                {
                    Glide.with(mainActivity).load(images.get(0).getImage_url()).into(holder.imageView1);
                    holder.textView1.setText(d.getLabel().toUpperCase());
                }
                else
                {

                    if (count == 1)
                    {
                        Glide.with(mainActivity).load(images.get(0).getImage_url()).into(holder.imageView2);
                        holder.textView2.setText(d.getLabel().toUpperCase());
                    }
                    else
                    {
                        Glide.with(mainActivity).load(images.get(0).getImage_url()).into(holder.imageView3);
                    }
                }

            }
            count++;
        }
    }

    @Override
    public int getItemCount()
    {
        Log.d("size",""+objectDataList.size());
        return objectDataList.size();

    }

    public class MyViewHolder extends RecyclerView.ViewHolder
    {
        public TextView sectionHeader;
        ImageView imageView1,imageView2,imageView3;
        TextView textView,textView1,textView2;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            imageView1=(ImageView) itemView.findViewById(R.id.imageView1);
            imageView2=(ImageView) itemView.findViewById(R.id.imageView2);
            imageView3=(ImageView) itemView.findViewById(R.id.imageView3);
            textView1=(TextView) itemView.findViewById(R.id.tv1);
            textView2=(TextView) itemView.findViewById(R.id.tv2);
            textView=(TextView) itemView.findViewById(R.id.tv3);
        }
    }

    public void  setObjectList(List<ObjectData>objectList )
    {
        this.objectDataList = objectList;
        notifyDataSetChanged();
    }
}